# Report for PDS Collections

The raw reports are produced with [collection_metadata.py](../collection_metadata.py).

## PDS/VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1

```bash
python collection_metadata.py --dc=PDS --ds=VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1 -v
```

- Repository path: `/volumes/kronos/voyager/data/pra/PDS/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1`
- File Count: 4
- File = `PRA_I.TAB` / `PRA_I.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-04-25 00:00:04`
   - End Time = `1979-05-28 23:59:14`
   - Number of file records = 32707
- File = `PRA_II.TAB` / `PRA_II.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-05-29 00:00:02`
   - End Time = `1979-06-23 23:59:59`
   - Number of file records = 34207
- File = `PRA_III.TAB` / `PRA_III.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-06-24 00:00:47`
   - End Time = `1979-07-12 23:59:58`
   - Number of file records = 31652
- File = `PRA_IV.TAB` / `PRA_IV.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-07-13 00:00:46`
   - End Time = `1979-08-04 23:05:33`
   - Number of file records = 34416
- Total Number of records = 132982

## PDS/VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1

```bash
python collection_metadata.py --dc=PDS --ds=VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1 -v
```

- Repository path: `/volumes/kronos/voyager/data/pra/PDS/VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1`
- File Count: 1
- File = `PRA.TAB` / `PRA.LBL`
   - Dataset Name = `VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1980-11-11 22:09:23`
   - End Time = `1980-11-16 23:59:47`
   - Number of file records = 6228
- Total Number of records = 6228

## PDS/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1

```bash
python collection_metadata.py --dc=PDS --ds=VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1 -v
```

- Repository path: `/volumes/kronos/voyager/data/pra/PDS/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1`
- File Count: 4
- File = `PRA_I.TAB` / `PRA_I.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-04-25 00:00:04`
   - End Time = `1979-05-28 23:59:14`
   - Number of file records = 32707
- File = `PRA_II.TAB` / `PRA_II.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-05-29 00:00:02`
   - End Time = `1979-06-23 23:59:59`
   - Number of file records = 34207
- File = `PRA_III.TAB` / `PRA_III.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-06-24 00:00:47`
   - End Time = `1979-07-12 23:59:58`
   - Number of file records = 31652
- File = `PRA_IV.TAB` / `PRA_IV.LBL`
   - Dataset Name = `VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1979-07-13 00:00:46`
   - End Time = `1979-08-04 23:05:33`
   - Number of file records = 34416
- Total Number of records = 132982

## PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1

```bash
python collection_metadata.py --dc=PDS --ds=VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1 -v
```

- Repository path: `/volumes/kronos/voyager/data/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1`
- File Count: 6
- File = `PRA.TAB` / `PRA.LBL`
   - Dataset Name = `VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1981-08-24 00:00:47`
   - End Time = `1981-08-31 22:05:34`
   - Number of file records = 13928
- File = `PRA_I.TAB` / `PRA_I.LBL`
   - Dataset Name = `VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1981-06-05 00:00:07`
   - End Time = `1981-06-30 23:59:16`
   - Number of file records = 37485
- File = `PRA_II.TAB` / `PRA_II.LBL`
   - Dataset Name = `VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1981-07-01 00:00:04`
   - End Time = `1981-07-21 23:59:13`
   - Number of file records = 31873
- File = `PRA_III.TAB` / `PRA_III.LBL`
   - Dataset Name = `VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1981-07-22 00:00:01`
   - End Time = `1981-08-13 23:59:12`
   - Number of file records = 37592
- File = `PRA_IV.TAB` / `PRA_IV.LBL`
   - Dataset Name = `VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1981-08-14 00:00:00`
   - End Time = `1981-09-07 23:59:58`
   - Number of file records = 36831
- File = `PRA_V.TAB` / `PRA_V.LBL`
   - Dataset Name = `VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1981-09-08 00:00:46`
   - End Time = `1981-09-28 23:59:08`
   - Number of file records = 34874
- Total Number of records = 192583

### Notes:
- **Inconsistent File Coverage**: File `PRA.TAB` temporal coverage included in
that of `PRA_IV.TAB`. 

### Further analysis:
The script below checks if the content of file1 is present (and identical) 
in file2. Errors are reported if 
- a time stamp of file1 is not found in file2, 
- the data content for time stamps in both files are not identical. 

```
>>> from pathlib import Path
>>> file1 = Path('/volumes/kronos/voyager/data/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1/DATA/PRA.LBL')
>>> file2 = Path('/volumes/kronos/voyager/data/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1/DATA/PRA_IV.LBL')
>>> import collection_metadata
>>> collection_metadata.compare_data_content('PDS', str(file1), str(file2))
Loading file1: /volumes/kronos/voyager/data/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1/DATA/PRA.LBL
file1: 111424 sweeps.
Loading file2: /volumes/kronos/voyager/data/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1/DATA/PRA_IV.LBL
file2: 294648 sweeps.
Processing...
Reported number of content inconstencies: 0
```

**Result**: All `PRA.TAB` records are found (and identical) in `PRA_IV.TAB`.

## PDS/VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1

```bash
python collection_metadata.py --dc=PDS --ds=VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1 -v
```

- Repository path: `/volumes/kronos/voyager/data/pra/PDS/VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1`
- File Count: 1
- File = `VG2_URN_PRA_6SEC.TAB` / `VG2_URN_PRA_6SEC.LBL`
   - Dataset Name = `VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1986-01-19 00:00:00`
   - End Time = `1986-01-31 00:00:00`
   - Number of file records = 22461
- Total Number of records = 22461

## PDS/VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1

```bash
python collection_metadata.py --dc=PDS --ds=VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1 -v
```

- Repository path: `/volumes/kronos/voyager/data/pra/PDS/VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1`
- File Count: 1
- File = `VG2_NEP_PRA_6SEC.TAB` / `VG2_NEP_PRA_6SEC.LBL`
   - Dataset Name = `VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1.0`
   - Start Time = `1989-08-11 00:00:00`
   - End Time = `1989-08-31 00:00:00`
   - Number of file records = 36028
- Total Number of records = 36028

