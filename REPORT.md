# Report on MASER Voyager PRA collections

The Planetary Radio Astronomy (PRA) recievers onboard the Voyager 1 and 2 
spacecraft have observerd for first time the low frequency radio spectra of 
the Jupiter, Saturn, Uranus and Neptune. They are thus a important piece of 
history for low frequency radio astronomy of our solar system exploration. 



## Data Collections

### PDS data Collection

*Source*: NASA/PDS/PPI node web site.

*Last update*: Apr. 2020

| PDS Dataset Name | MASER Dataset | Start Time | Stop Time | Notes |
| ---------------- | ------------- | ---------- | --------- | ----- |
| [`VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1.0)   | [`PDS/VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1)   | `1979-01-06 16:00:34` | `1979-04-14 15:59:08` | |
| [`VG1-J-PRA-4-SUMM-BROWSE-48SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG1-J-PRA-4-SUMM-BROWSE-48SEC-V1.0) | [`PDS/VG1-J-PRA-4-SUMM-BROWSE-48SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG1-J-PRA-4-SUMM-BROWSE-48SEC-V1) | `1979-01-07 00:00:48` | `1979-04-14 23:58:24` | |
| [`VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1.0)   | [`PDS/VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1)   | `1980-11-12 06:09:23` | `1980-11-17 07:59:47` | |
| [`VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1.0)   | [`PDS/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1)   | `1979-04-25 08:00:04` | `1979-08-05 06:05:33` | |
| [`VG2-J-PRA-4-SUMM-BROWSE-48SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-J-PRA-4-SUMM-BROWSE-48SEC-V1.0) | [`PDS/VG2-J-PRA-4-SUMM-BROWSE-48SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-J-PRA-4-SUMM-BROWSE-48SEC-V1) | `1979-04-25 08:00:00` | `1979-08-05 06:04:00` | |
| [`VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1.0)   | [`PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1)   | `1981-06-05 14:00:07` | `1981-09-29 13:59:08` | |
| [`VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1.0)   | [`PDS/VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1)   | `1986-01-19 08:00:00` | `1986-02-01 07:59:59` | (1) |
| [`VG2-U-PRA-2-RDR-HIGHRATE-60MS-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-U-PRA-2-RDR-HIGHRATE-60MS-V1.0) | [`PDS/VG2-U-PRA-2-RDR-HIGHRATE-60MS-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-U-PRA-2-RDR-HIGHRATE-60MS-V1) | `1985-10-25 06:23:12` | `1988-07-14 14:43:09` | |
| [`VG2-U-PRA-4-SUMM-BROWSE-48SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-U-PRA-4-SUMM-BROWSE-48SEC-V1.0) | [`PDS/VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG1-J-PRA-3-RDR-LOWBAND-6SEC-V1)   | `1986-01-19 08:00:00` | `1986-02-01 07:59:59` | |
| [`VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1.0)   | [`PDS/VG2-U-PRA-4-SUMM-BROWSE-48SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-U-PRA-4-SUMM-BROWSE-48SEC-V1) | ~~`1977-08-21 05:53:34`~~ | ~~`2007-09-01 14:00:00`~~ | (1,2) |
| [`VG2-N-PRA-2-RDR-HIGHRATE-60MS-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-N-PRA-2-RDR-HIGHRATE-60MS-V1.0) | [`PDS/VG2-N-PRA-2-RDR-HIGHRATE-60MS-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-N-PRA-2-RDR-HIGHRATE-60MS-V1) | `1989-05-25 13:58:32` | `1989-09-28 00:52:43` | |
| [`VG2-N-PRA-4-SUMM-BROWSE-48SEC-V1.0`](https://pds-ppi.igpp.ucla.edu/search/view/?f=yes&id=pds://PPI/VG2-N-PRA-4-SUMM-BROWSE-48SEC-V1.0) | [`PDS/VG2-N-PRA-4-SUMM-BROWSE-48SEC-V1`](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-N-PRA-4-SUMM-BROWSE-48SEC-V1) | `1989-08-18 07:00:00` | `1989-09-07 06:59:57` | |

Notes:

1.  `VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1` and `VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1` are marked as *Uncertified* datasets by NASA/PDS.
2.  `VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1` *Start Time* and *Stop Time* from NASA/PDS/PPI repository are incorrect. Fixed in MASER hosted labels.

### GSFC data Collection

*Source*: M.L. Kaiser (NASA/GSFC, retired)

*Last Update*: DVD media recieved in 2011

| MASER Dataset Name | Start Time | Stop Time |
| ------------------ | ---------- | --------- |
| [`GSFC/VG1-J-PRA-LOWBAND-6SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG1-J-PRA-LOWBAND-6SEC-MLK)   |  |  | 
| [`GSFC/VG1-J-PRA-LOWBAND-48SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG1-J-PRA-LOWBAND-48SEC-MLK) |  |  | 
| [`GSFC/VG1-S-PRA-LOWBAND-6SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG1-S-PRA-LOWBAND-6SEC-MLK)   |  |  | 
| [`GSFC/VG1-S-PRA-LOWBAND-48SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG1-S-PRA-LOWBAND-48SEC-MLK) |  |  | 
| [`GSFC/VG2-J-PRA-LOWBAND-6SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-J-PRA-LOWBAND-6SEC-MLK)   |  |  | 
| [`GSFC/VG2-J-PRA-LOWBAND-48SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-J-PRA-LOWBAND-48SEC-MLK) |  |  | 
| [`GSFC/VG2-N-PRA-LOWBAND-6SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-N-PRA-LOWBAND-6SEC-MLK)   |  |  | 
| [`GSFC/VG2-N-PRA-LOWBAND-48SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-N-PRA-LOWBAND-48SEC-MLK) |  |  | 
| [`GSFC/VG2-S-PRA-LOWBAND-6SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-S-PRA-LOWBAND-6SEC-MLK)   |  |  | 
| [`GSFC/VG2-S-PRA-LOWBAND-48SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-S-PRA-LOWBAND-48SEC-MLK) |  |  | 
| [`GSFC/VG2-U-PRA-LOWBAND-6SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-U-PRA-LOWBAND-6SEC-MLK)   |  |  | 
| [`GSFC/VG2-U-PRA-LOWBAND-48SEC-MLK`](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-U-PRA-LOWBAND-48SEC-MLK) |  |  | 


### NSSDC data Collection

*Source*: M.L. Kaiser (NASA/GSFC, retired)

*Last Update*: DVD media recieved in 2011

| MASER Dataset Name | Start Time | Stop Time |
| ------------------ | ---------- | --------- |
| [`NSSDC/PSFP-00059`](http://maser.obspm.fr/data/voyager/pra/NSSDC/PSFP-00059) | | |
| [`NSSDC/PSFP-00236`](http://maser.obspm.fr/data/voyager/pra/NSSDC/PSFP-00236) | | | 

### University of Iowa data Collection

*Source*: Univ. of Iowa web site.

*Last update*: Feb. 2018

| UIowa Dataset Name | MASER Dataset Name | Start Time | Stop Time |
| ------------------ | ------------------ | ---------- | --------- |
| [`JPT1H`](http://www-pw.physics.uiowa.edu/voyager/data/pra/JPT1H/) | [`UIowa/VG1-J-PRA-RAW-HIGHBAND-6SEC-V0`](http://maser.obspm.fr/data/voyager/pra/UIowa/VG1-J-PRA-RAW-HIGHBAND-6SEC-V0) |  |  |
| [`JPT2H`](http://www-pw.physics.uiowa.edu/voyager/data/pra/JPT2H/) | [`UIowa/VG2-J-PRA-RAW-HIGHBAND-6SEC-V0`](http://maser.obspm.fr/data/voyager/pra/UIowa/VG2-J-PRA-RAW-HIGHBAND-6SEC-V0) |  |  |
| [`saturn`](http://www-pw.physics.uiowa.edu/voyager/data/pra/saturn/) | [`UIowa/VG1-S-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/UIowa/VG1-S-PRA-RAW-HIGHBAND-6SEC-V1) <br> [`UIowa/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1`](http://maser.obspm.fr/data/voyager/pra/UIowa/VG2-S-PRA-RAW-HIGHBAND-6SEC-V1)| <br> | <br> |

### CNES data Collection

*Source*: SERAD (archive service)

*Last Update*: Oct. 2015

| Dataset Name | URL | Spacecraft | Planet | Mode | Resolution |
| ------------ | --- | ---------- | ------ | ---- | ----------:|


## Comparing Science Data Content

### PDS and GSFC collections
This part is done using the script `cross_check_gsfc_pds_collections.py`

| PDS Collection  | GSFC collection     | Description | Formatting | Content | Comments |
| --------------- | ------------------- | ----------- | ---------- | ------- | -------- |
| [VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-J-PRA-3-RDR-LOWBAND-6SEC-V1) | [VG2-J-PRA-LOWBAND-6SEC-MLK](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-J-PRA-LOWBAND-6SEC-MLK) | VG2 Jupiter flyby data | Differs | Differs | |
| [VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1) | [VG2-N-PRA-LOWBAND-6SEC-MLK](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-N-PRA-LOWBAND-6SEC-MLK) | VG2 Neptune flyby data | Differs | Identical | |
| [VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-S-PRA-3-RDR-LOWBAND-6SEC-V1) | [VG2-S-PRA-LOWBAND-6SEC-MLK](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-S-PRA-LOWBAND-6SEC-MLK) | VG2 Saturn flyby data | Differs | Differs | |
| [VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1](http://maser.obspm.fr/data/voyager/pra/PDS/VG2-U-PRA-3-RDR-LOWBAND-6SEC-V1) | [VG2-U-PRA-LOWBAND-6SEC-MLK](http://maser.obspm.fr/data/voyager/pra/GSFC/VG2-U-PRA-LOWBAND-6SEC-MLK) | VG2 Uranus flyby data | Differs | Identical | | 

