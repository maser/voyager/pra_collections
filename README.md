# MASER Voyager PRA Collections 

This code repository includes scripts used to compare the science content of the Voyager/PRA collections 
gathered in the MASER repository.

The code scripts are using the data reader modules of the 
[Maser4py](https://github.com/maserlib/maser4py) library, as available in its 
`feature/data` branch.

_**This repository is work in progress.**_ 

## References
- Cecconi, B., L. Lamy, P. Zarka (**2020**). *MASER Voyager-PRA Repository 
Collection* (Version 1.0) [Data set]. PADC. 
[doi:10.25935/PK0T-5W59](https://doi.org/10.25935/PK0T-5W59).
- Cecconi, B., A. Loh, P. Le Sidaner, R. Savalle, X. Bonnin, Q.-N. Nguyen, 
S. Lion, et al. (**2020**). *MASER: a Science Ready Toolbox for Low Frequency Radio 
Astronomy*. Data Science Journal 19 (18): 1062. 
[doi:10.5334/dsj-2020-012](https://doi.org/10.5334/dsj-2020-012).
