#!/usr/bin/env python

import sys
from pathlib import Path
from maser.data.padc.lesia.voyager.pra_gsfc import GSFCVoyagerPRAData as gsfc_vg_pra_lb_6s
from maser.data.pds.voyager.pra import PDSPPIVoyagerPRARDRLowBand6SecDataFromLabel as pds_vg_pra_lb_6s
import numpy


repo_attributes = {
    'PDS': {
        'data_dir': 'DATA',
        'file_ext': 'TAB',
        'str_date_indices': (0,12),
        'maser_loader': pds_vg_pra_lb_6s,
    },
    'GSFC': {
        'data_dir': 'DATA',
        'file_ext': 'dat',
        'str_date_indices': (2,14),
        'maser_loader': gsfc_vg_pra_lb_6s,
    },
}

local_repository = Path('/volumes/kronos/voyager/data/pra')


def compare_data_content(repo_name, file1, file2, verbose=False):

    print(f'Loading file1: {file1}')
    o1 = repo_attributes[repo_name]['maser_loader'](str(file1))
    print(f'file1: {len(o1)} sweeps.')

    print(f'Loading file2: {file2}')
    o2 = repo_attributes[repo_name]['maser_loader'](str(file2))
    print(f'file2: {len(o2)} sweeps.')

    print('Processing...')

#    time1 = o1.get_time_axis()
#    time2 = o2.get_time_axis()
    error_cnt = 0
    for i1, t1 in enumerate(o1.time):
        if t1 in o2.time:
           i2 = int(numpy.where(o2.time == t1)[0])
#           print(i2, type(i2))
           s1 = o1.get_single_sweep(i1)
           s2 = o2.get_single_sweep(i2)
           if numpy.all(numpy.equal(s1.raw_sweep, s2.raw_sweep)):
               if verbose:
                   print(f'{t1} [{i1}:{i2}] : ok')
           else:
               print(f'{t1} [{i1}:{i2}] : data differs :: {s1.raw_sweep-s2.raw_sweep}')
               error_cnt += 1
        else:
            print(f'{t1} not found in file2: missing data.')
            error_cnt += 1

    print(f'Reported number of content inconstencies: {error_cnt}')


def collection_metadata(repo_name, collection_name, verbose=False):

    repo_path = local_repository / repo_name / collection_name
    if repo_path.exists():
        print(f'- Repository path: `{repo_path}`')
    else:
        raise FileNotFoundError("Can't find repoistory")

    files = sorted(list((repo_path / repo_attributes[repo_name]['data_dir']).glob(f"*.{repo_attributes[repo_name]['file_ext']}")))
    print(f'- File Count: {len(files)}')
    global_cnt = 0
    checks = {}
    for item in files:
        if repo_name == 'PDS':
            cur_file = item.parent / item.name.replace('.TAB', '.LBL')
        else:
            cur_file = item
        o = repo_attributes[repo_name]['maser_loader'](str(cur_file))
        if repo_name == 'PDS':
            o.load_data('TABLE')
            print(f'- File = `{item.name}` / `{o.get_file_name()}`')
        else:
            print(f'- File = `{o.get_file_name()}`')
        checks[o.get_file_name()] = {'start_time': o.start_time, 'end_time': o.end_time}
        print(f'   - Dataset Name = `{o.dataset_name}`')
        print(f'   - Start Time = `{o.start_time}`')
        print(f'   - End Time = `{o.end_time}`')
        print(f'   - Number of file records = {o.label["FILE_RECORDS"]}')
        print(f'   - Number of sweeps = {o.nsweep}')
        global_cnt += int(o.label["FILE_RECORDS"])
#        print(f'Number of sweeps = {len(o.object["TABLE"].data)}')

    print(f'- Total Number of records = {global_cnt}')


if __name__ == "__main__": 

    argv = sys.argv[1:]

    verbose = False
    for arg in argv:
       if arg == '-v':
           verbose = True
       if arg.startswith('--dc='):
           repository_name = arg.replace('--dc=', '')
       if arg.startswith('--ds='):
           collection_name = arg.replace('--ds=', '')

    collection_metadata(repository_name, collection_name, verbose=verbose)


