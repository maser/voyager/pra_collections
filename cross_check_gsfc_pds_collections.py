#!/usr/bin/env python
from pathlib import Path
import sys

"""
python cross_check_gsfc_pds_collections.py --pds=/volumes/kronos/voyager/data/pra/PDS/VG2-N-PRA-3-RDR-LOWBAND-6SEC-V1/DATA/ --gsfc=/volumes/kronos/voyager/data/pra/GSFC/VG2-N-PRA-LOWBAND-6SEC-MLK/DATA/
"""

repo_attributes = {
    'PDS': {
        'file_ext': 'TAB',
        'str_date_indices': (0,12)
    },
    'GSFC': {
        'file_ext': 'dat',
        'str_date_indices': (2,14)
    },
}


def extract_repository_input_string(repo_string):
    if ':' in repo_string:
        repo_directory, tmp = repo_string.split(':')
        repo_files = tmp.split(',')
    else:
        repo_directory = repo_string
        repo_files = None
    return repo_directory, repo_files


def line_count(files):
    cnt = 0
    for item in files:
        with open(item, 'r') as f:
            cnt += len(list(f.readlines()))
    return cnt


def get_files_from_repo(repo_path, repo_name, repo_files):
    if repo_files is None:
        files = sorted(list(Path(repo_path).glob(f'*.{repo_attributes[repo_name]["file_ext"]}')))
    else:
        files = []
        for item in sorted(repo_files):
            files.append(Path(repo_path) / item)
    return files


def diff(str1, str2):
   dispatch = {True: '-', False: 'O'}
   strd = ''
   lmax = max((len(str1), len(str2)))
   for i in range(lmax):
      try:
         strd = strd + dispatch(str1[i] == str1[2])
      except:
         strd = strd + 'X'
   return strd


def cross_check_repo(repo_path1, repo_name1, repo_files1, repo_path2, repo_name2, repo_files2, verbose=False):

    files1 = get_files_from_repo(repo_path1, repo_name1, repo_files1)
    files2 = get_files_from_repo(repo_path2, repo_name2, repo_files2)

    total_line_cnt1 = line_count(files1)
    total_line_cnt2 = line_count(files2)

    print(f'Line count check. {repo_name1}: {total_line_cnt1} / {repo_name2}: {total_line_cnt2}')

    date_str_start1, date_str_stop1 = repo_attributes[repo_name1]["str_date_indices"]
    date_str_start2, date_str_stop2 = repo_attributes[repo_name2]["str_date_indices"]

    error_cnt = 0

    cur_fidx2 = 0
    len_files2 = len(files2)
    if verbose:
        print(f'{len_files2} files in {repo_name2}')
    item2 = files2[cur_fidx2]
    f2 = open(item2, 'r')
    if verbose:
        print(f'Opening file: {item2} ({repo_name2})')
    lines2 = f2.readlines()
    len_lines2 = len(lines2)
    cur_lidx2 = 0
    for cur_fidx1, item1 in enumerate(files1):
        with open(item1, 'r') as f1:
            if verbose:
                print(f'Opening file: {item1} ({repo_name1})')
            lines1 = f1.readlines()
            for cur_lidx1, line1 in enumerate(lines1):
                if verbose:
                    print(f'{repo_name1}: {cur_lidx1+1:06d}/{len(lines1):06d} {cur_fidx1+1:02d}/{len(files1):02d} - ' + 
                          f'{repo_name2}: {cur_lidx2+1:06d}/{len_lines2:06d} {cur_fidx2+1:02d}/{len_files2:02d}')
                date1 = line1[date_str_start1:date_str_stop1]
                date2 = lines2[cur_lidx2][date_str_start2:date_str_stop2]
                if date1 != date2:
                    error_cnt += 1
                    print(f'{repo_name1}: {date1}')
                    print(f'{repo_name2}: {date2}')
                    input('ok?')
                else:
                    data1 = line1[date_str_stop1:]
                    data2 = lines2[cur_lidx2][date_str_stop2:]
                    if data1 != data2:
                        error_cnt += 1
                        print(f'{repo_name1:<5}: {date1} - {data1}')
                        print(f'{repo_name2:<5}: {date2} - {data2}')
                        print(f' diff:              - {diff(data1, data2)}')
#                        input('ok?')
                    cur_lidx2 += 1
                    if cur_lidx2 == len_lines2:
                        if verbose:
                            print(f'Finished reading {item2} ({repo_name2})')
#                            input('ok?')
                        f2.close()
                        cur_fidx2 += 1
                        if cur_fidx2 < len_files2:
                            item2 = files2[cur_fidx2]
                            f2 = open(item2, 'r')
                            if verbose:
                                print(f'Opening file: {item2} ({repo_name2})')
                            lines2 = f2.readlines()
                            len_lines2 = len(lines2)
                            cur_lidx2 = 0
    if error_cnt == 0:
        print('No error detected')
    return error_cnt

if __name__ == "__main__": 

    verbose = False
    argv = sys.argv[1:]
    for item in argv:
        if item == '-v':
            verbose = True
        if item.startswith('--pds='):
            pds_repo_directory, pds_files = extract_repository_input_string(item.replace('--pds=', ''))
        if item.startswith('--gsfc='):
            gsfc_repo_directory, gsfc_files = extract_repository_input_string(item.replace('--gsfc=', ''))

    forward_error_cnt = cross_check_repo(
        pds_repo_directory, 'PDS', pds_files,
        gsfc_repo_directory, 'GSFC', gsfc_files,
        verbose=verbose
    )

    reverse_error_cnt = cross_check_repo(
        gsfc_repo_directory, 'GSFC', gsfc_files,
        pds_repo_directory, 'PDS', pds_files,
        verbose=verbose
    )

    if forward_error_cnt == 0 and reverse_error_cnt == 0:
        print("Data content of repositories are identical, no error detected")
